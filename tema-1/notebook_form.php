<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notebook Form</title>
</head>
<body>
<div class="row">
	<div class="col-12">
		<h1>Agregar</h1>
		<form action="./controllers/notebook.php" method="POST">
			<div class="form-group">
				<label for="marca">Marca ID</label>
				<input required name="marca" type="number" id="marca" placeholder="Id de marca" class="form-control">
			</div>
			<div class="form-group">
				<label for="pais">Pais ID</label>
				<input required name="pais" type="number" id="pais" placeholder="Id de pais" class="form-control">
			</div>
			<div class="form-group">
				<label for="modelo">Modelo</label>
				<input required name="modelo" type="text" id="modelo" placeholder="Modelo de notebook" class="form-control">
			</div>
			<div class="form-group">
				<label for="ram">Memoria Ram</label>
				<input required name="ram" type="text" id="ram" placeholder="Ram de notebook" class="form-control">
			</div>
			<div class="form-group">
				<label for="disco">Disco Duro</label>
				<input required name="disco" type="text" id="disco" placeholder="Disco duro de notebook" class="form-control">
			</div>
			<div class="form-group">
				<label for="usb">Cantidad Puertos USB</label>
				<input required name="usb" type="number" id="usb" placeholder="Puertos de notebook" class="form-control">
			</div>
			<div class="form-group">
				<label for="procesador">Procesador</label>
				<input required name="procesador" type="text" id="procesador" placeholder="Disco duro de notebook" class="form-control">
			</div>
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="./notebooks.php" class="btn btn-warning">Ver todas</a>
		</form>
	</div>
</div>
</body>
</html>