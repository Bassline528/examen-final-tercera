<?php

class DBInterface {
    

    public function connect() {
        $password = "admin";
        $user = "ingesoft8_user";
        $dbname = "finalTercera";
        $host = "127.0.0.1";
        $port = "5432";
        try {
            $db = new PDO("pgsql:host=$host;port=$port;dbname=$dbname", $user, $password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $db;
        } catch (Exception $e) {
            echo "Ocurrió un error con la base de datos: " . $e->getMessage();
        }
        
    }
}


?>