<?php
    require_once "db/config.php";

    $dbinterface = new DBInterface();
    $db = $dbinterface->connect();

    $query = $db->query("select * from notebooks");
    $notebooks = $query->fetchAll(PDO::FETCH_OBJ);


    echo "<h2>LISTADO DE NOTEBOOKS</h2>";
    echo "<table>";
    echo "<tr>";
    echo "<th>ID</th>";
    echo "<th>Modelo</th>";
    echo "<th>Procesador</th>";
    echo "<th>Memoria Ram</th>";
    echo "</tr>";
    foreach ($notebooks as $key => $value) {
        echo "<tr>";
        echo "<td>$value->id</td>";
        echo "<td>$value->modelo></td>";
        echo "<td>$value->procesador</td>";
        echo "<td>$value->memoria_ram</td>";
        echo "</tr>";
    }
    
    echo "</table>";
?>