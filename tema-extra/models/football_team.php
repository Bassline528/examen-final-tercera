<?php


    class FootballTeam {
        private string $fullName;
        private $players;

        public function __construct(string $fullName) {
            $this->fullName = $fullName;
        }

        public function setPlayers(array $players = []) {
            $this->players = $players;
        }

        public function getFullTeamDescription() {
            echo "Equipo $this->fullName";
            foreach ($this->players as $key => $value) {
                echo "\n";

                $value->getFullInfo();
            }
            echo "\n";
        }

    }
?> 