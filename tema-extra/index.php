<?php
    include_once 'models/player.php';
    include_once 'models/football_team.php';

    $team1 = new FootballTeam('Olimpia');
    $team2 = new FootballTeam('Cerro Portño');

    $player1 = new FootballPlayer();
    $player1->setFullName('Nelson');
    $player1->setAge(21);
    $player1->setPosition('Delantero');
   
    $player2 = new FootballPlayer();
    $player2->setFullName('Matias');
    $player2->setAge(20);
    $player2->setPosition('Arquero');
   
    $player3 = new FootballPlayer();
    $player3->setFullName('Cuajo');
    $player3->setAge(21);
    $player3->setPosition('Defensor');
   
    $player4 = new FootballPlayer();
    $player4->setFullName('Renato');
    $player4->setAge(21);
    $player4->setPosition('Medio');
   
    $player5 = new FootballPlayer();
    $player5->setFullName('David');
    $player5->setAge(21);
    $player5->setPosition('Delantero');
   
    $player6 = new FootballPlayer();
    $player6->setFullName('Oscar');
    $player6->setAge(21);
    $player6->setPosition('Arquero');
   
    $player7 = new FootballPlayer();
    $player7->setFullName('Spaini');
    $player7->setAge(21);
    $player7->setPosition('Medio');
   
    $player8 = new FootballPlayer();
    $player8->setFullName('Oliver');
    $player8->setAge(21);
    $player8->setPosition('Medio');
 

    $team1->setPlayers([$player1,$player2,$player3,$player4]);
    $team2->setPlayers([$player5,$player6,$player7,$player8]);

    $team1->getFullTeamDescription();
    $team2->getFullTeamDescription();

?>